import { SearchOutlined } from "@material-ui/icons";

import FavoriteIcon from "@material-ui/icons/Favorite";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";

import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Product = ({ item }) => {
  return (
    <Container>
      <Circle />
      <Image src={item.img} />
      <Info>
        <Icon color="#4dbb0d" bg="#f0fced">
          <ShoppingCartIcon />
        </Icon>
        <Icon color="#0c6ac2" bg="#dff1f7">
          <Link to={`/product/${item._id}`} style={{ textDecoration: "none" }}>
            <SearchOutlined />
          </Link>
        </Icon>
        <Icon color="#f0280d" bg="#fcf0f0">
          <FavoriteIcon />
        </Icon>
      </Info>
    </Container>
  );
};

const Container = styled.div`
  flex: 1;
  margin: 7px;
  min-width: 280px;
  height: 350px;
  background-color: #fff3f0;
  border-radius: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  border: 1px solid #fce2d8;
  transition: all 0.4s ease;

  &:hover {
    border: 1px solid #ffffff;
  }
`;

const Circle = styled.div`
  width: 200px;
  height: 200px;
  border-radius: 50%;
  background-color: #ffffff;
  position: absolute;
  z-index: 0;
`;

const Image = styled.img`
  height: 75%;
  z-index: 1;
`;

const Info = styled.div`
  opacity: 0;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: rgba(78, 78, 78, 0.3);
  border-radius: 20px;
  z-index: 2;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all 0.4s ease;
  cursor: pointer;

  &:hover {
    opacity: 1;
  }
`;

const Icon = styled.div`
  width: 35px;
  height: 35px;
  margin: 0 15px;
  padding: 10px;
  background-color: #ffffff;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all 0.3s ease;
  cursor: pointer;

  &:hover,
  &:active {
    color: ${(props) => props.color};
    background-color: ${(props) => props.bg};
    transform: scale(1.16);
    box-shadow: 3px 3px rgba(0, 0, 0, 0.1);
  }
`;

export default Product;
