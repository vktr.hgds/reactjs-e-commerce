import styled from "styled-components";
import React, { useEffect, useState } from "react";
import Aos from "aos";
import "aos/dist/aos.css";

import Product from "./Product";
import axios from "axios";

const Products = ({ cat, filters, sort, title }) => {
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);

  useEffect(() => {
    Aos.init({
      mirror: false,
    });
  }, []);

  // get by category through backend api
  useEffect(() => {
    const getProducts = async () => {
      try {
        const response = await axios.get(
          cat
            ? `http://localhost:5000/api/products?category=${cat}`
            : "http://localhost:5000/api/products"
        );
        setProducts(response.data);
      } catch (err) {}
    };

    getProducts();
  }, [cat]);

  // get by filters on client side
  useEffect(() => {
    cat &&
      setFilteredProducts(
        products.filter((item) =>
          Object.entries(filters).every(([key, value]) =>
            item[key].includes(value)
          )
        )
      );
  }, [products, cat, filters]);

  // sort products on client sideű
  useEffect(() => {
    if (sort === "newest") {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => a.createdAt - b.createdAt)
      );
    } else if (sort === "asc") {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => a.price - b.price)
      );
    } else {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => b.price - a.price)
      );
    }
  }, [sort]);

  return (
    <Container id="products">
      {title && (
        <InfoContainer>
          <Info data-aos="fade-up-right" data-aos-duration="1000">
            {title}
            <Border data-aos="fade-down-right" data-aos-duration="1300" />
          </Info>
        </InfoContainer>
      )}
      <Wrapper data-aos="fade-left" data-aos-duration="1600">
        {cat
          ? filteredProducts.map((item) => (
              <Product item={item} key={item.id} />
            ))
          : products
              .slice(0, 8)
              .map((item) => <Product item={item} key={item.id} />)}
      </Wrapper>
    </Container>
  );
};

const Container = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const InfoContainer = styled.div`
  width: 100%;
  padding: 20px 0px 40px 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const Info = styled.span`
  letter-spacing: 2px;
  position: relative;
  font-size: 40px;
  font-weight: 500;
  padding-bottom: 8px;
`;

const Border = styled.div`
  border-bottom: 4px solid #5fa3db;
  position: absolute;
  left: 50px;
  right: 50px;
  bottom: 0;
`;

export default Products;
