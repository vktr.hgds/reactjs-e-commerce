import React, { useState } from "react";
import styled from "styled-components";
import { Search, ShoppingCartOutlined } from "@material-ui/icons";

import { Badge } from "@material-ui/core";
import { mobile } from "../../../responsive";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const Navbar = () => {
  const [navbar, setNavbar] = useState(false);
  const quantity = useSelector((state) => state.cart.quantity);

  return (
    <Container navbar={navbar}>
      <Wrapper>
        <Left>
          <Language>EN</Language>
          <SearchContainer navbar={navbar}>
            <Input navbar={navbar} placeholder="Search" />
            <SearchIcon>
              <Search />
            </SearchIcon>
          </SearchContainer>
        </Left>
        <Center>
          <StyledLink to="/" style={{ textDecoration: "none" }}>
            <Logo>QQ.</Logo>
          </StyledLink>
        </Center>
        <Right>
          <MenuItem>
            <SearchIcon />
          </MenuItem>
          <MenuItem color="#a8a13f">REGISTER</MenuItem>
          <MenuItem>SIGN IN</MenuItem>
          <MenuItem>
            <Badge
              badgeContent={quantity}
              color={navbar ? "secondary" : "primary"}
            >
              <ShoppingCartOutlined />
            </Badge>
          </MenuItem>
        </Right>
      </Wrapper>
    </Container>
  );
};

export default Navbar;

const mainBgColor = "#ffffff";

const Container = styled.div`
  height: 60px;
  border-bottom: 1px solid #b9b9b9;
  background-color: ${mainBgColor};
  position: sticky;
  position: -webkit-sticky;
  top: 0;
  bottom: 0;
  left: 0;
  z-index: 4;
  transition: all 0.3s ease-in-out;
`;

const Wrapper = styled.div`
  padding: 10px 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${mobile({ padding: "10px 0px" })}
`;

const Left = styled.div`
  flex: 1;
  display: flex;
  align-items: center;

  ${mobile({ flex: 2.5 })}
`;

const SearchContainer = styled.div`
  border: ${(props) => (props.navbar ? "1px solid #8fbfce" : "1px solid gray")};
  display: flex;
  align-items: center;
  margin-left: 25px;
  padding: 5px;
  border-radius: 6px;
`;

const Input = styled.input`
  border: none;
  font-size: 16px;
  background-color: ${mainBgColor};
  transition: all 0.3s ease-in-out;

  &:focus {
    outline: none;
  }

  ${mobile({ width: "70px" })}
`;

const SearchIcon = styled.div`
  cursor: pointer;
  transition: all 0.2s ease;

  &:hover {
    color: gray;
  }
`;

const Language = styled.span`
  font-size: 14px;
  cursor: pointer;

  ${mobile({
    display: "none",
  })}
`;

const Center = styled.div`
  flex: 1;
  text-align: center;
`;

const Image = styled.img`
  width: 135px;
  height: 57px;
  object-fit: cover;
  cursor: pointer;
`;

const Logo = styled.h1`
  font-weight: bold;
  text-decoration: none;

  ${mobile({ fontSize: "24px", marginLeft: "10px" })}
`;

const Right = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  ${mobile({ justifyContent: "center", flex: 3 })}
`;

const MenuItem = styled.div`
  font-size: 16px;
  cursor: pointer;
  margin-left: 30px;
  transition: all 0.5s ease-in-out;

  &:hover,
  &:active {
    color: ${(props) => props.color};
  }

  ${mobile({ fontSize: "12px", marginRight: "10px", marginLeft: "0px" })}
`;

const StyledLink = styled(Link)`
  text-decoration: none;

  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`;
