import styled from "styled-components";

const Announcement = () => {
  return <Container>Super Deal! Free shipping on orders over $50!</Container>;
};

export default Announcement;

const Container = styled.div`
  height: 40px;
  background-color: teal;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 5px;
  font-size: 16px;
  font-weight: 500;
`;
