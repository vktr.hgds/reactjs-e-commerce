import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { mobile } from "../../../responsive";

const buttonColor = "#5592a7";

const CategoryItem = ({ item }) => {
  return (
    <Container>
      <Link to={`/products/${item.cat}`}>
        <Image src={item.img} />
        <Info>
          <Title>{item.title}</Title>
          <Button>SHOP NOW</Button>
        </Info>
      </Link>
    </Container>
  );
};

const Container = styled.div`
  flex: 1;
  margin: 7px;
  height: 60vh;
  position: relative;
`;

const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 20px;
  border: 1px solid #b8b8b8;

  ${mobile({ height: "20vh", borderRadius: "5px" })}
`;

const Info = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`;

const Title = styled.h1`
  color: white;
  margin-bottom: 10px;
`;

const Button = styled.button`
  color: ${buttonColor};
  border: 2px solid ${buttonColor};
  background-color: white;
  padding: 12px;
  font-size: 18px;
  border-radius: 20px;
  cursor: pointer;
  transition: all 0.2s ease;

  &:hover,
  &:active {
    background-color: ${buttonColor};
    color: white;
    font-weight: 700;
    border: 2px solid white;
  }
`;

export default CategoryItem;
