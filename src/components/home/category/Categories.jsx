import styled from "styled-components";
import React, { useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";

import { categories } from "../../../data";
import CategoryItem from "./CategoryItem";
import { mobile } from "../../../responsive";

const Categories = () => {
  return (
    <Container id="categories">
      <InfoContainer>
        <Info>
          CATEGORIES
          <Border />
        </Info>
      </InfoContainer>
      <Wrapper>
        {categories.map((item) => (
          <CategoryItem item={item} key={item.id} />
        ))}
      </Wrapper>
    </Container>
  );
};

const Container = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  background-color: #f7f7f7;

  ${mobile({ padding: "0px" })}
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;

  ${mobile({ flexDirection: "column" })}
`;

const InfoContainer = styled.div`
  width: 100%;
  padding: 20px 0px 40px 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const Info = styled.span`
  letter-spacing: 2px;
  position: relative;
  font-size: 40px;
  font-weight: 500;
  padding-bottom: 8px;
`;

const Border = styled.div`
  border-bottom: 4px solid #5fa3db;
  position: absolute;
  left: 50px;
  right: 50px;
  bottom: 0;
`;

export default Categories;
