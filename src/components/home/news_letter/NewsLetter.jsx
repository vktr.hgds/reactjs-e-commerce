import { Send } from "@material-ui/icons";
import React from "react";
import styled from "styled-components";
import { mobile } from "../../../responsive";

const NewsLetter = () => {
  return (
    <Container>
      <Title>Newsletter</Title>
      <Description>Get timely updates from your favourite products</Description>
      <InputContainer>
        <Input placeholder="Your email" />
        <Button>
          <Send></Send>
        </Button>
      </InputContainer>
    </Container>
  );
};

const Container = styled.div`
  height: 60vh;
  background-color: #ffffe4;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const Title = styled.span`
  font-size: 70px;
  margin-bottom: 20px;

  ${mobile({ textAlign: "center", fontSize: "50px" })}
`;

const Description = styled.span`
  font-size: 24px;
  font-weight: 300;
  margin-bottom: 20px;

  ${mobile({ textAlign: "center", fontSize: "16px" })}
`;

const InputContainer = styled.div`
  width: 40%;
  height: 40px;
  background-color: white;
  display: flex;
  justify-content: center;
  border: 0.5px solid #ddd;
  border-radius: 10px;

  ${mobile({ width: "90%" })}
`;

const Input = styled.input`
  font-size: 20px;
  padding-left: 20px;
  flex: 20;
  border: none;
  border-radius: 10px 0px 0px 10px;

  &:focus {
    outline: none;
  }
`;

const Button = styled.button`
  flex: 1;
  cursor: pointer;
  background-color: #2b2b2b;
  color: #ffffff;
  border: none;
  transition: all 0.2s ease;
  border-radius: 0px 10px 10px 0px;

  &:hover {
    background-color: #dbdbdb;
    color: #000000;
  }
`;

export default NewsLetter;
