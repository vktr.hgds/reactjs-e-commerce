import axios from "axios";

const BASE_URL = "http://localhost:5000/api/";
const TOKEN =
  "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo1OSwiZXhwIjoxNjM4MTg4ODQ3fQ.Sq3bBpKvn6hVPvxcKq0cfvUGKQkZF91VgdpTDtlCq1A";
const REFRESH_TOKEN =
  "6f01d5724a4bbc1c7aa39e90f4b36d9ab16af12a1ea1ac8c293ed5ec169beb97";
const ROLE = "admin";

export const publicRequest = axios.create({
  baseURL: BASE_URL,
});

export const userRequest = axios.create({
  baseURL: BASE_URL,
  header: { token: `Bearer ${TOKEN}` },
});
