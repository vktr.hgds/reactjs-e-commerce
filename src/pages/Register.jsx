import styled from "styled-components";
import { mobile } from "../responsive";

const Register = () => {
  return (
    <Container>
      <Wrapper>
        <Title>CREATE ACCOUNT</Title>
        <Form>
          <Input placeholder="first name"></Input>
          <Input placeholder="last name"></Input>
          <Input placeholder="username"></Input>
          <Input placeholder="email"></Input>
          <Input placeholder="password"></Input>
          <Input placeholder="confirm password"></Input>
          <Agreement>
            By creating an account, I consent to the processing of my personal
            data in accordance with the <b>private policy</b>.
          </Agreement>
          <Button>CREATE</Button>
        </Form>
      </Wrapper>
    </Container>
  );
};

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(#f3f3f3, #fff8f8);

  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  padding: 20px;
  width: 40%;
  background: white;
  border-radius: 10px;
  border: 1px solid #d8d8d8;

  ${mobile({ width: "80%" })}
`;

const Title = styled.span`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  padding: 10px;
  margin: 20px 10px 0 0;
  border: 0.5px solid #ddd;
  border-radius: 5px;
  transition: 0.3s all ease;

  &:active,
  &:hover,
  &:focus {
    border: 1px solid teal;
    outline: none;
  }
`;

const Agreement = styled.span`
  font-size: 12px;
  margin: 20px 0px;
`;

const Button = styled.button`
  width: 40%;
  border: none;
  padding: 15px 20px;
  background-color: teal;
  color: white;
  font-size: 16px;
  cursor: pointer;
  border: 2px solid white;
  transition: all 0.2s ease;
  border-radius: 10px;

  &:hover,
  &:active {
    background-color: teal;
    border: 2px solid teal;
    background-color: white;
    color: teal;
  }
`;

export default Register;
