import styled from "styled-components";
import { mobile } from "../responsive";

const Login = () => {
  return (
    <Container>
      <Wrapper>
        <Title>SIGN IN</Title>
        <Form>
          <Input placeholder="email"></Input>
          <Input placeholder="password"></Input>
          <Button>SIGN IN</Button>

          <Link>Create a new account</Link>
          <Link>Forgot password</Link>
        </Form>
      </Wrapper>
    </Container>
  );
};

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(#f3f3f3, #fff6f6);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  padding: 20px;
  width: 40%;
  background: white;
  border-radius: 10px;
  border: 1px solid #d8d8d8;

  ${mobile({ width: "80%" })}
`;

const Title = styled.span`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  padding: 10px;
  margin: 10px 0px;
  border: 0.5px solid #ddd;
  border-radius: 5px;
  transition: all 0.3s ease;

  &:active,
  &:hover,
  &:focus {
    border: 0.5px solid teal;
    outline: none;
  }
`;

const Button = styled.button`
  width: 40%;
  border: none;
  padding: 10px;
  margin: 10px 0;
  background-color: teal;
  color: white;
  font-size: 16px;
  cursor: pointer;
  border: 2px solid white;
  transition: all 0.2s ease;
  border-radius: 10px;

  &:hover,
  &:active {
    background-color: teal;
    border: 2px solid teal;
    background-color: white;
    color: teal;
  }

  ${mobile({ padding: "10px" })}
`;

const Link = styled.a`
  margin: 5px 0;
  font-size: 12px;
  text-decoration: underline;
  cursor: pointer;
`;

export default Login;
