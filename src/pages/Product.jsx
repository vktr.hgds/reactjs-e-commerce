import { Add, Remove } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useLocation } from "react-router";
import styled from "styled-components";
import Footer from "../components/home/footer/Footer";
import Navbar from "../components/home/navbar/Navbar";
import NewsLetter from "../components/home/news_letter/NewsLetter";
import { addProduct } from "../redux/cartRedux";
import { publicRequest } from "../requestMethods";
import { mobile } from "../responsive";

const Product = () => {
  const location = useLocation();
  const id = location.pathname.split("/")[2];
  const [product, setProduct] = useState({});
  const [quantity, setQuantity] = useState(1);
  const [color, setColor] = useState("");
  const [size, setSize] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    const getProduct = async () => {
      const response = await publicRequest.get(`/products/find/${id}`);
      setProduct(response.data);
    };

    getProduct();
  }, [id]);

  const handleQuantity = (type) => {
    if (type === "decrease") {
      quantity > 1 && setQuantity(quantity - 1);
    } else {
      setQuantity(quantity + 1);
    }
  };

  const handleColor = (color) => {
    setColor(color);
  };

  const handleSize = (e) => {
    setSize(e.target.value);
  };

  // redux method to add product to cart
  const handleClick = () => {
    dispatch(addProduct({ ...product, quantity, color, size }));
  };

  return (
    <>
      <Navbar />
      <Wrapper>
        <Left>
          <Image src={product.img} />
        </Left>
        <Right>
          <Title>{product.title}</Title>
          <Desc>{product.desc}</Desc>
          <PriceContainer>
            <Price>$ {product.price}</Price>
          </PriceContainer>
          <FilterContainer>
            <Filter>
              <FilterTitle>Color:</FilterTitle>
              {product?.color?.map((c) => (
                <FilterColor color={c} key={c} onClick={() => handleColor(c)} />
              ))}
            </Filter>
            <Filter>
              <FilterTitle>Size:</FilterTitle>
              <FilterSize onClick={handleSize}>
                {product?.size?.map((size) => (
                  <FilterSizeOption key={size}>{size}</FilterSizeOption>
                ))}
              </FilterSize>
            </Filter>
          </FilterContainer>
          <AddContainer>
            <AmountContainer>
              <Remove onClick={() => handleQuantity("decrease")} />
              <Amount>{quantity}</Amount>
              <Add onClick={() => handleQuantity("increase")} />
            </AmountContainer>
            <Button onClick={handleClick}>ADD TO CART</Button>
          </AddContainer>
        </Right>
      </Wrapper>
      <NewsLetter />
      <Footer />
    </>
  );
};

const Wrapper = styled.div`
  padding: 50px;
  display: flex;

  ${mobile({ flexDirection: "column", padding: "10px" })}
`;

const Left = styled.div`
  flex: 1;
`;

const Image = styled.img`
  width: 100%;
  height: 90vh;
  object-fit: cover;
  border-radius: 30px 0px;

  ${mobile({ height: "40vh" })}
`;

const Right = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;

  padding: 0 50px;

  ${mobile({ padding: "0px" })}
`;

const Title = styled.h1`
  font-weight: 200;
`;

const Desc = styled.span`
  margin: 20px 0px;
`;

const PriceContainer = styled.div`
  width: 120px;
  height: 60px;
  background-color: #2d9142;
  border-radius: 10px;
  display: flex;
  align-items: center;
  justify-content: center;

  ${mobile({ width: "100px" })}
`;

const Price = styled.span`
  font-weight: 300;
  font-size: 35px;
  color: #ffffff;

  ${mobile({ fontSize: "24px" })}
`;

const FilterContainer = styled.div`
  width: 50%;
  margin: 30px 0;
  display: flex;
  justify-content: space-between;

  ${mobile({ width: "100%" })}
`;

const Filter = styled.div`
  display: flex;
  align-items: center;

  ${mobile({ marginBottom: "10px" })}
`;

const FilterTitle = styled.span`
  font-size: 20px;
  font-weight: 200;
  padding-right: 20px;
`;

const FilterColor = styled.div`
  width: 25px;
  height: 25px;
  background-color: ${(props) => props.color};
  margin: 0 10px;
  border-radius: 50%;
  cursor: pointer;

  ${mobile({ margin: "0px 5px" })}
`;

const FilterSize = styled.select`
  margin-left: 10px;
  padding: 10px;
`;

const FilterSizeOption = styled.option``;

const AddContainer = styled.div`
  width: 50%;
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${mobile({ width: "100%" })}
`;

const AmountContainer = styled.div`
  display: flex;
  align-items: center;
  font-weight: 700;
`;

const Amount = styled.span`
  width: 40px;
  height: 40px;
  align-content: center;
  border-radius: 10px;
  border: 1px solid gray;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 10px;
`;

const Button = styled.button`
  padding: 10px;
  border: 2px solid teal;
  background-color: white;
  cursor: pointer;
  transition: all 0.2s ease;
  border-radius: 10px;

  &:hover,
  &:active {
    background-color: teal;
    color: white;
  }

  ${mobile({ width: "130px", height: "40px" })}
`;

export default Product;
