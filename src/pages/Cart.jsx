import { Add, Remove } from "@material-ui/icons";
import styled from "styled-components";
import Footer from "../components/home/footer/Footer";
import Navbar from "../components/home/navbar/Navbar";
import { mobile } from "../responsive";

const Cart = () => {
  return (
    <Container>
      <Navbar />
      <Wrapper>
        <Title>YOUR BAG</Title>
        <Top>
          <TopButton>CONTINUE SHOPPING</TopButton>
          <TopTexts>
            <TopText>Shopping cart (2)</TopText>
            <TopText>Wishlist (1)</TopText>
          </TopTexts>
          <TopButton color="black" bgColor="white">
            CHECKOUT NOW
          </TopButton>
        </Top>
        <Bottom>
          <Info>
            <Product>
              <ProductDetail>
                <Image src="https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1614188818-TD1MTHU_SHOE_ANGLE_GLOBAL_MENS_TREE_DASHERS_THUNDER_b01b1013-cd8d-48e7-bed9-52db26515dc4.png?crop=1xw:1.00xh;center,top&resize=480%3A%2A" />
                <Details>
                  <ProductName>
                    <b>Product:</b> JESSIE THUNDER SHOES
                  </ProductName>
                  <ProductId>
                    <b>ID:</b> 34324324324324
                  </ProductId>
                  <ProductColor color="red" />
                  <ProductSize>
                    <b>Size:</b> 37.5
                  </ProductSize>
                </Details>
              </ProductDetail>
              <PriceDetail>
                <ProductAmountContainer>
                  <AmountContainer color="red">
                    <Remove />
                  </AmountContainer>
                  <ProductAmount>2</ProductAmount>
                  <AmountContainer color="green">
                    <Add />
                  </AmountContainer>
                </ProductAmountContainer>
                <ProductPrice>$ 34.98</ProductPrice>
              </PriceDetail>
            </Product>
            <Product>
              <ProductDetail>
                <Image src="https://hips.hearstapps.com/vader-prod.s3.amazonaws.com/1614188818-TD1MTHU_SHOE_ANGLE_GLOBAL_MENS_TREE_DASHERS_THUNDER_b01b1013-cd8d-48e7-bed9-52db26515dc4.png?crop=1xw:1.00xh;center,top&resize=480%3A%2A" />
                <Details>
                  <ProductName>
                    <b>Product:</b> JESSIE THUNDER SHOES
                  </ProductName>
                  <ProductId>
                    <b>ID:</b> 34324324324324
                  </ProductId>
                  <ProductColor color="gray" />
                  <ProductSize>
                    <b>Size:</b> 37.5
                  </ProductSize>
                </Details>
              </ProductDetail>
              <PriceDetail>
                <ProductAmountContainer>
                  <AmountContainer color="red">
                    <Remove />
                  </AmountContainer>
                  <ProductAmount>2</ProductAmount>
                  <AmountContainer color="green">
                    <Add />
                  </AmountContainer>
                </ProductAmountContainer>
                <ProductPrice>$ 34.98</ProductPrice>
              </PriceDetail>
            </Product>
          </Info>
          <Summary>
            <SummaryTitle>ORDER SUMMARY</SummaryTitle>
            <SummaryItem>
              <SummaryItemText>Subtotal</SummaryItemText>
              <SummaryItemPrice>$ 79.96</SummaryItemPrice>
            </SummaryItem>
            <SummaryItem>
              <SummaryItemText>Estimated shipping</SummaryItemText>
              <SummaryItemPrice>$ 5.90</SummaryItemPrice>
            </SummaryItem>
            <SummaryItem>
              <SummaryItemText>Shipping discount</SummaryItemText>
              <SummaryItemPrice>$ -4.90</SummaryItemPrice>
            </SummaryItem>
            <SummaryItem type="total">
              <SummaryItemText>Total</SummaryItemText>
              <SummaryItemPrice>$ 81.96</SummaryItemPrice>
            </SummaryItem>
            <SummaryButton>CHECKOUT NOW</SummaryButton>
          </Summary>
        </Bottom>
      </Wrapper>
      <Footer />
    </Container>
  );
};

const Container = styled.div`
  height: 100vh;
`;

const Wrapper = styled.div`
  padding: 30px;

  ${mobile({ padding: "10px" })}
`;

const Title = styled.h1`
  font-weight: 300;
  text-align: center;
`;

const Top = styled.div`
  margin-top: 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const TopButton = styled.button`
  margin: 10px 0px;
  padding: 10px;
  font-weight: 600;
  cursor: pointer;
  color: ${(props) => (props.color ? props.color : "white")};
  background: ${(props) => (props.bgColor ? props.bgColor : "black")};
  border: 1px solid ${(props) => (props.color ? props.color : "white")};
  transition: all 0.3s ease;
  border-radius: 7px;

  &:active,
  &:hover {
    color: ${(props) => (props.color ? "white" : "black")};
    background: ${(props) => (props.bgColor ? "black" : "white")};
    border: 1px solid black;
  }

  ${mobile({ fontSize: "10px" })}
`;

const TopTexts = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const TopText = styled.span`
  font-size: 14px;
  margin: 10px;
  padding: 10px;
  font-weight: 300;
  text-decoration: underline;
  cursor: pointer;

  ${mobile({ fontSize: "12px" })}
`;

const Bottom = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  ${mobile({ flexDirection: "column" })}
`;

const Info = styled.div`
  flex: 3;
`;

const Product = styled.div`
  background: #fafafa;
  border: 1px solid rgb(231, 231, 231);
  border-radius: 5px;
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
  padding-bottom: 10px;

  ${mobile({ flexDirection: "column", position: "relative" })};
`;

const ProductDetail = styled.div`
  flex: 2;
  display: flex;
  padding-bottom: 5px;
`;

const Image = styled.img`
  width: 200px;

  ${mobile({ width: "100px" })}
`;

const Details = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;

  ${mobile({ padding: "5px" })}
`;

const ProductName = styled.span`
  ${mobile({ fontSize: "14px" })}
`;

const ProductId = styled.span`
  ${mobile({ fontSize: "12px" })}
`;

const ProductColor = styled.div`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background: ${(props) => props.color};
`;

const ProductSize = styled.span`
  ${mobile({ fontSize: "12px" })}
`;

const PriceDetail = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  ${mobile({ flexDirection: "row", justifyContent: "space-around" })}
`;

const ProductAmountContainer = styled.div`
  display: flex;
  align-items: center;
`;

const ProductAmount = styled.span`
  padding: 10px;
  margin: 10px;
  border-radius: 5px;
  border: 1px solid black;
  background: white;
  font-weight: 300;
  font-size: 24px;
  width: 30px;
  height: 30px;
  text-align: center;

  ${mobile({
    padding: "5px",
    margin: "5px",
    fontSize: "16px",
    width: "20px",
    height: "20px",
  })}
`;

const AmountContainer = styled.div`
  color: black;
  cursor: pointer;
  transition: all 0.2s ease;

  &:active,
  &:hover {
    color: ${(props) => props.color};
  }
`;

const ProductPrice = styled.span`
  background: #2d9142;
  padding: 10px;
  border-radius: 5px;
  color: white;
  font-weight: 400;
  margin-top: 20px;
  font-size: 20px;

  ${mobile({
    marginTop: "0px",
    fontSize: "14px",
    padding: "8px",
    position: "absolute",
    bottom: "0",
    right: "0",
    borderRadius: "5px 0px 5px 0px",
  })}
`;

const Summary = styled.div`
  flex: 1;
  border: 1px solid #ddd;
  border-radius: 10px;
  height: 50vh;
  margin-left: 20px;
  padding: 20px;

  ${mobile({ marginLeft: "0px" })}
`;

const SummaryTitle = styled.h1`
  font-weight: 200;
`;

const SummaryItem = styled.div`
  margin: 30px 0px;
  display: flex;
  justify-content: space-between;
  font-weight: ${(props) => props.type === "total" && 500};
  font-size: ${(props) => props.type === "total" && 24}px;
`;

const SummaryItemText = styled.span``;

const SummaryItemPrice = styled.span``;

const SummaryButton = styled.button`
  margin: 10px 0px;
  padding: 10px;
  font-weight: 600;
  cursor: pointer;
  color: white;
  background: teal;
  border: 2px solid white;
  transition: all 0.3s ease;
  border-radius: 7px;
  width: 100%;

  &:active,
  &:hover {
    color: black;
    background: white;
    border: 2px solid black;
  }
`;

export default Cart;
