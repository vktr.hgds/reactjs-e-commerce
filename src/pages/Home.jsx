import Navbar from "../components/home/navbar/Navbar";
import Announcement from "../components/home/announcement/Announcement";
import Slider from "../components/home/slider/Slider";
import Categories from "../components/home/category/Categories";
import Products from "../components/home/product/Products";
import NewsLetter from "../components/home/news_letter/NewsLetter";
import Footer from "../components/home/footer/Footer";

const Home = () => {
  return (
    <>
      <Navbar />
      <Slider />
      <Categories />
      <Products title="TOP PRODUCTS" />
      <NewsLetter />
      <Footer />
    </>
  );
};

export default Home;
