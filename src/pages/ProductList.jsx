import { useState } from "react";
import { useLocation } from "react-router";
import styled from "styled-components";
import Footer from "../components/home/footer/Footer";
import Navbar from "../components/home/navbar/Navbar";
import NewsLetter from "../components/home/news_letter/NewsLetter";
import Products from "../components/home/product/Products";
import { mobile } from "../responsive";

const ProductList = () => {
  const location = useLocation();
  const category = location.pathname.split("/")[2];
  const [filters, setFilters] = useState({});
  const [sort, setSort] = useState("newest");

  const handleFilters = (e) => {
    const value = e.target.value;
    setFilters({
      ...filters,
      [e.target.name]: value,
    });
  };

  const handleSort = (e) => {
    setSort(e.target.value);
  };

  console.log(sort);

  console.log(filters);
  return (
    <>
      <Navbar />
      <Title>{category}</Title>

      <FilterContainer>
        <Filter>
          <FilterText>Filter products:</FilterText>
          <Select name="color" onChange={handleFilters}>
            <Option disabled defaultValue>
              Color
            </Option>
            <Option>white</Option>
            <Option>black</Option>
            <Option>red</Option>
            <Option>blue</Option>
            <Option>yellow</Option>
            <Option>green</Option>
          </Select>
          <Select name="size" onChange={handleFilters}>
            <Option disabled defaultValue>
              Size
            </Option>
            <Option>XS</Option>
            <Option>S</Option>
            <Option>M</Option>
            <Option>L</Option>
            <Option>XL</Option>
            <Option>XXL</Option>
          </Select>
        </Filter>
        <Filter>
          <FilterText>Sort products:</FilterText>
          <Select onChange={handleSort}>
            <Option defaultValue value="newest">
              Newest
            </Option>
            <Option value="asc">Price (ascending)</Option>
            <Option value="desc">Price (descending)</Option>
          </Select>
        </Filter>
      </FilterContainer>
      <Products cat={category} filters={filters} sort={sort} />
      <NewsLetter />
      <Footer />
    </>
  );
};

const Title = styled.h1`
  margin: 20px;
`;

const FilterContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Filter = styled.div`
  margin: 20px;

  ${mobile({ margin: "0px 20px", display: "flex", flexDirection: "column" })}
`;

const FilterText = styled.span`
  font-size: 20px;
  font-weight: 600;
  margin-right: 20px;

  ${mobile({ marginRight: "0px", fontSize: "14px" })}
`;

const Select = styled.select`
  margin-right: 20px;
  padding: 10px;

  &:focus {
    text-decoration: none;
  }

  ${mobile({ margin: "10px 0px 0px 0px" })}
`;

const Option = styled.option`
  margin: 20px;
`;

export default ProductList;
